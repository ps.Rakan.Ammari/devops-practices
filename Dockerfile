FROM openjdk:19-jdk

WORKDIR /app
COPY target/*.jar /app

ENV ARG_A "-jar"
ENV ARG_B "-Dspring.profiles.active=h2"

CMD java $ARG_A $ARG_B *.jar